from dataclasses import dataclass

@dataclass
class Pool:
  x: float 
  y: float  
  fee_percent: float  

  def k(self) -> float:
    return self.x * self.y

  def fee_factor(self) -> float:
    return (100 - self.fee_percent) / 100

  def swap_x_for_y(self, x_in: float) -> float:
    f = self.fee_factor()
    x_in_with_fee = x_in * f
    new_x = self.x + x_in_with_fee
    new_y = self.k() / new_x
    y_out = self.y - new_y

    self.x += x_in
    self.y = new_y

    return y_out

  def swap_y_for_x(self, y_in: float) -> float:
    f = self.fee_factor()
    y_in_with_fee = y_in * f
    new_y = self.y + y_in_with_fee
    new_x = self.k() / new_y
    x_out = self.x - new_x

    self.y += y_in
    self.x = new_x

    return x_out

  def x_spot_price(self) -> float:
    return self.y / self.x

  def y_spot_price(self) -> float:
    return self.x / self.y